function createstar(center, radius,) {
    var star = document.createElement('div');
    star.className = 'star';
    star.style.width = `${radius}px`;
    star.style.height = `${radius}px`;
    star.style.top = `${center.top-radius/2}px`;
    star.style.left = `${center.left-radius/2}px`;

    if (radius > 500){
        star.classList('shine');
    }
    return star;
}

 let cont = document.getElementById('d');
for (let i=0; i<600; i++){
    cont.appendChild(createstar({
        top: getRandom(0, cont.offsetHeight),
        left: getRandom(0, cont.offsetWidth)
        }, getRandom(1, 2), ));
}

function getRandom(min, max, notFloor) {
    return(!notFloor && Math.floor || (a=>a))(Math.random()*(max - min + 1)+min);

}
